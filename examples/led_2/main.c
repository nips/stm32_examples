/*
 * Copyright (c) 2018 Maciej Duniec.
 * author Maciej Duniec <mduniec@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "stm32f10x.h"
#include "include/snippets.h"

void setup() {
    // enable APB2 peripheral clock
    RCC->APB2ENR = RCC_APB2ENR_IOPBEN;

    // reset default states
    GPIOB->CRL &= ~GPIO_CRL_CNF;
    // GPIOB->CRH &= ~GPIO_CRH_CNF;

    // set PB0 to input mode
    pinMode(GPIOB, 0, gpio_input_pull);
    // set PB0 input to pull
    GPIOB->ODR |= GPIO_ODR_ODR0;

    // set PB1 to output mode push-pull, 2Mhz
    pinMode(GPIOB, 1, gpio_output_pushpull_2MHz);

    // set PB7 to output mode push-pull, 2Mhz
    pinMode(GPIOB, 5, gpio_output_pushpull_2MHz);

}

int main(void){

    setup();

    while(1){
        if (digitalRead(GPIOB, 0)) {
            digitalWrite(GPIOB, 5, LOW);
            digitalWrite(GPIOB, 1, HIGH);
        } else {
            digitalWrite(GPIOB, 5, HIGH);
            digitalWrite(GPIOB, 1, LOW);
        }
    }
}
