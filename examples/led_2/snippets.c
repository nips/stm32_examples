/*
 * Copyright (c) 2018 Maciej Duniec.
 * author Maciej Duniec <mduniec@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "include/snippets.h"

void digitalWrite(GPIO_TypeDef *gpio_port, int pin, int state) {
    if (state) {
        // atomic operation
        // write to first 16 bits on BSRR register
        // 1 in BSRR sets 1 to ODR register, 0 is ignored
        gpio_port->BSRR = 1<<pin; // atomic
    } else {
        // atomic operation
        // write to first 16 bits on BRR register
        // 1 in BRR sets 0 to ODR register, 0 is ignored
        gpio_port->BRR = 1<<pin; // atomic
    }
}

uint32_t digitalRead(GPIO_TypeDef *gpio_port, int pin) {
    return gpio_port->IDR & (1<<pin);
}

void pinMode(GPIO_TypeDef *gpio_port, int pin, GpioMode_t mode) {
    // for pins 0..7 register CRL
    // for pins 8..15 register CRH

    if (pin<8) {
        gpio_port->CRL |= (uint32_t) mode<<(pin*4);
    } else {
        gpio_port->CRH |= (uint32_t) mode<<(pin*4);
    }
}

void delay() {
    volatile uint32_t d;
    for(d = 1000000; d; d--){};
}
