/*
 * Copyright (c) 2018 Maciej Duniec.
 * author Maciej Duniec <mduniec@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "stm32f10x.h"

#define HIGH 1
#define LOW 0

void delay() {
    volatile uint32_t d;
    for(d = 1000000; d; d--){};
}

void digitalWrite(GPIO_TypeDef *gpio_port, int pin, int state) {
    if (state) {
        // atomic operation
        // write to first 16 bits on BSRR register
        // 1 in BSRR sets 1 to ODR register, 0 is ignored
        gpio_port->BSRR = 1<<pin; // atomic
    } else {
        // atomic operation
        // write to first 16 bits on BRR register
        // 1 in BRR sets 0 to ODR register, 0 is ignored
        gpio_port->BRR = 1<<pin; // atomic
    }
}

void setup() {
    // enable APB2 peripheral clock
    RCC->APB2ENR = RCC_APB2ENR_IOPBEN;

    // set PB1 to output mode push-pull, 2Mhz
    GPIOB->CRL = ((uint32_t)0b00100000);
    // alternative
    // GPIOB->CRL |= GPIO_CRL_MODE1_1;
    // GPIOB->CRL &= ~GPIO_CRL_CNF1_0;
}


int main(void){

    setup();

    while(1){
        digitalWrite(GPIOB, 1, HIGH);
        delay();
        digitalWrite(GPIOB, 1, LOW);
        delay();
    }
}
