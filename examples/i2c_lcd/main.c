/*
 * Copyright (c) 2018 Maciej Duniec.
 * author Maciej Duniec <mduniec@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "stm32f10x.h"
#include "include/snippets.h"
#define PCF8574_ADDR (0x3F)


void I2C_Init(void) {
    // Software reset I2C
    I2C1->CR1 |= I2C_CR1_SWRST;
    I2C1->CR1 &= ~I2C_CR1_SWRST;

    I2C1->CR1 &= ~I2C_CR1_PE; // deactivate i2c (for configuration i2c must by deactivated)

    I2C1->CR1 &= ~I2C_CR1_SMBUS; //SMBus Mode = I2C
    I2C1->CR2 &= ~I2C_CR2_DMAEN; //DMA deactivate
    I2C1->CR2 &= ~I2C_CR2_ITBUFEN; // buffer interrupt disable
    I2C1->CR2 &= ~I2C_CR2_ITEVTEN; // event interrupt disable
    I2C1->CR2 &= ~I2C_CR2_ITERREN; // error interrupt disable
    I2C1->CR2 = ((I2C1->CR2 & ~I2C_CR2_FREQ) | (0x10)); // peripheral clock frequency

    I2C1->CCR &= ~I2C_CCR_FS; // standard mode I2C
    I2C1->CCR |= 0x50;  // 100kHz
    I2C1->TRISE |= 17;

    // Wlaczenie I2C
    I2C1->CR1 |= I2C_CR1_PE | I2C_CR1_ACK;
    I2C1->OAR1 = 1<<14;
    I2C1->OAR2 = 1<<3;
}

void I2C_start(void) {
    volatile uint16_t *i2c1_sr1 = &I2C1->SR1;
    I2C1->CR1 |= I2C_CR1_START;
    while(!(I2C1->SR1 & I2C_SR1_SB));
    uint32_t dummy = I2C1->SR2;
}

void I2C_stop(void) {
    while((I2C1->SR1 & I2C_SR1_BTF) != I2C_SR1_BTF);
    I2C1->CR1 |= I2C_CR1_STOP;
}

void I2C_send_address(uint8_t address) {
    volatile uint16_t *i2c1_sr1 = &I2C1->SR1;
    I2C1->DR = (address<<1);// | ~(1<<0);
    while(!(I2C1->SR1 & I2C_SR1_ADDR));
    uint32_t dummy = I2C1->SR1;
    dummy = I2C1->SR2;
}

void I2C_send_byte(uint8_t byte) {
    while(!(I2C1->SR1 & I2C_SR1_TXE));
    I2C1->DR = byte;
}

void I2C_write(uint8_t adresx, uint16_t length, uint16_t *buf) {
    if ((I2C1->SR2 & I2C_SR2_BUSY) == I2C_SR2_BUSY);

    I2C_start();
    I2C_send_address(adresx);
    while (length--) {
        I2C_send_byte(*buf++);
        delay();
    }

    I2C_stop();

}

void setup() {
    // enable APB2 peripheral clock
    RCC->APB2ENR = RCC_APB2ENR_IOPBEN;

    // enable i2c 1 clock
    RCC->APB1ENR = RCC_APB1ENR_I2C1EN;

    // reset default states
    GPIOB->CRL &= ~GPIO_CRL_CNF;

    pinMode(GPIOB, 6, gpio_alternate_drain_2MHz);
    pinMode(GPIOB, 7, gpio_alternate_drain_2MHz);

    // set PB1 to output mode push-pull, 2Mhz
    pinMode(GPIOB, 1, gpio_output_pushpull_2MHz);

    // set PB7 to output mode push-pull, 2Mhz
    pinMode(GPIOB, 5, gpio_output_pushpull_2MHz);

    I2C_Init();

}

int main(void){
    RCC->APB2ENR = RCC_APB2ENR_IOPBEN;
    setup();

    I2C_start();
    I2C_send_address(0x3F);
    I2C_send_byte(0b00001100);
    delay();
    I2C_send_byte(0b00001111);
    delay();
    I2C_send_byte(0b00000001);
    delay();
    I2C_send_byte(0b00000111);
    I2C_stop();

    /*
    uint16_t dummy[] = {
        0b00001100,
        0b00001111,
        0b00000001,
        0b00000111
    };

    I2C_write(0x3F, sizeof(dummy), dummy);
    delay();
    */
    delay();
    delay();
    delay();
    delay();
    delay();

    digitalWrite(GPIOB, 1, HIGH);

    while(1) {
    }
}
