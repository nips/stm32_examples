/*
 * Copyright (c) 2018 Maciej Duniec.
 * author Maciej Duniec <mduniec@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SNIPPETS_H
#define __SNIPPETS_H

#include "stm32f10x.h"

#define HIGH 1
#define LOW 0
#define PIN_IN 0
#define PIN_OUT 1
#define PIN_ANALOG 2

typedef enum {
    /* output mode */
    /* push pull */
    gpio_output_pushpull_2MHz = 0b0010,
    gpio_output_pushpull_10MHz = 0b0001,
    gpio_output_pushpull_50MHz = 0b0011,
    /* open drain */
    gpio_output_drain_2MHz = 0b0110,
    gpio_output_drain_10MHz = 0b0101,
    gpio_output_drain_50MHz = 0b0111,

    /* alternate mode */
    /* push pull */
    gpio_alternate_pushpull_2MHz = 0b1010,
    gpio_alternate_pushpull_10MHz = 0b1001,
    gpio_alternate_pushpull_50MHz = 0b1011,
    /* open drain */
    gpio_alternate_drain_2MHz = 0b1110,
    gpio_alternate_drain_10MHz = 0b1101,
    gpio_alternate_drain_50MHz = 0b1111,

    /* input mode */
    /* input - analog (ADC) */
    gpio_input_analog = 0b0000,
    /* input - floating */
    gpio_input_floating = 0b0100,
    /* input pull-up/down - must set ODR additionally  */
    gpio_input_pull = 0b1000
} GpioMode_t;

void digitalWrite(GPIO_TypeDef *gpio_port, int pin, int state);
uint32_t digitalRead(GPIO_TypeDef *gpio_port, int pin);
void pinMode(GPIO_TypeDef *gpio_port, int pin, GpioMode_t mode);
void delay();

#endif
