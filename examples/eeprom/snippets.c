/*
 * Copyright (c) 2018 Maciej Duniec.
 * author Maciej Duniec <mduniec@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "include/snippets.h"

void digitalWrite(GPIO_TypeDef *gpio_port, int pin, int state) {
    if (state) {
        // atomic operation
        // write to first 16 bits on BSRR register
        // 1 in BSRR sets 1 to ODR register, 0 is ignored
        gpio_port->BSRR = 1<<pin; // atomic
    } else {
        // atomic operation
        // write to first 16 bits on BRR register
        // 1 in BRR sets 0 to ODR register, 0 is ignored
        gpio_port->BRR = 1<<pin; // atomic
    }
}

uint32_t digitalRead(GPIO_TypeDef *gpio_port, int pin) {
    return gpio_port->IDR & (1<<pin);
}

void pinMode(GPIO_TypeDef *gpio_port, int pin, GpioMode_t mode) {
    // for pins 0..7 register CRL
    // for pins 8..15 register CRH

    if (pin<8) {
        gpio_port->CRL |= (uint32_t) mode<<(pin*4);
    } else {
        gpio_port->CRH |= (uint32_t) mode<<(pin*4);
    }
}

void delay() {
    volatile uint32_t d;
    for(d = 100000; d; d--){};
}

void I2C_init(void) {
    // Software reset I2C
    I2C1->CR1 |= I2C_CR1_SWRST;
    I2C1->CR1 &= ~I2C_CR1_SWRST;

    I2C1->CR1 &= ~I2C_CR1_PE; // deactivate i2c (for configuration i2c must by deactivated)

    I2C1->CR1 &= ~I2C_CR1_SMBUS; //SMBus Mode = I2C
    I2C1->CR2 &= ~I2C_CR2_DMAEN; //DMA deactivate
    I2C1->CR2 &= ~I2C_CR2_ITBUFEN; // buffer interrupt disable
    I2C1->CR2 &= ~I2C_CR2_ITEVTEN; // event interrupt disable
    I2C1->CR2 &= ~I2C_CR2_ITERREN; // error interrupt disable
    I2C1->CR2 = ((I2C1->CR2 & ~I2C_CR2_FREQ) | (0x10)); // peripheral clock frequency

    I2C1->CCR &= ~I2C_CCR_FS; // standard mode I2C
    I2C1->CCR |= 0x50;  // 100kHz
    I2C1->TRISE |= 17;

    // Wlaczenie I2C
    I2C1->CR1 |= I2C_CR1_PE | I2C_CR1_ACK;
    I2C1->OAR1 = 1<<14;
    I2C1->OAR2 = 1<<3;
}

void I2C_start(void) {
    volatile uint32_t dummy = 0x0;
    volatile uint16_t *i2c1_sr1 = &I2C1->SR1;
    I2C1->CR1 |= I2C_CR1_START;
    while(!(I2C1->SR1 & I2C_SR1_SB));
    dummy = I2C1->SR2;
}

void I2C_stop(void) {
    while((I2C1->SR1 & I2C_SR1_BTF) != I2C_SR1_BTF);
    I2C1->CR1 |= I2C_CR1_STOP;
}

void I2C_send_header(uint8_t header) {
    volatile uint16_t *i2c1_sr1 = &I2C1->SR1;
    while(!(I2C1->SR1 & I2C_SR1_ADD10));
    uint32_t dummy = I2C1->SR1;
    dummy = I2C1->SR2;
}

void I2C_send_address(uint8_t address) {
    volatile uint16_t *i2c1_sr1 = &I2C1->SR1;
    I2C1->DR = address;
    while(!(I2C1->SR1 & I2C_SR1_ADDR));
    uint32_t dummy = I2C1->SR1;
    dummy = I2C1->SR2;
}

void I2C_send_byte(uint8_t byte) {
    while(!(I2C1->SR1 & I2C_SR1_TXE));
    I2C1->DR = byte;
}

uint8_t I2C_read_byte() {
    while(!(I2C1->SR1 & I2C_SR1_RXNE));
    I2C1->CR1 &= ~I2C_CR1_ACK;
    return I2C1->DR & 0xff;
}

void I2C_write(uint8_t adresx, uint16_t length, uint16_t *buf) {
    if ((I2C1->SR2 & I2C_SR2_BUSY) == I2C_SR2_BUSY);

    I2C_start();
    I2C_send_address(adresx);
    while (length--) {
        I2C_send_byte(*buf++);
        delay();
    }

    I2C_stop();
}

void write_eeprom_data(uint8_t mem_addr, uint8_t data) {
    I2C_start();
    // i2c address (write) - 0xa0 (0b10100000)
    I2C_send_address(0xa0);
    // memory address - where store data
    I2C_send_byte(mem_addr);
    I2C_send_byte(data);
    I2C_stop();
}

uint8_t read_eeprom_data(uint8_t mem_addr) {
    uint8_t data = 0x0;

    I2C_start();
    // i2c address (write) - 0xa0 (0b10100000)
    I2C_send_address(0xa0);
    I2C_send_byte(mem_addr);

    // delay();

    I2C_start();
    // i2c address (read) - 0xa1 (0b10100001)
    I2C_send_address(0xa1);
    data = I2C_read_byte();
    I2C_stop();

    return data;
}
