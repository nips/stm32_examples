/*
 * Copyright (c) 2018 Maciej Duniec.
 * author Maciej Duniec <mduniec@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "stm32f10x.h"
#include "include/snippets.h"

void setup() {
    // enable APB2 peripheral clock
    RCC->APB2ENR = RCC_APB2ENR_IOPBEN;

    // enable i2c 1 clock
    RCC->APB1ENR = RCC_APB1ENR_I2C1EN;

    // reset default states
    GPIOB->CRL &= ~GPIO_CRL_CNF;

    pinMode(GPIOB, 6, gpio_alternate_drain_2MHz);
    pinMode(GPIOB, 7, gpio_alternate_drain_2MHz);

    // set PB1 to output mode push-pull, 2Mhz
    pinMode(GPIOB, 1, gpio_output_pushpull_2MHz);

    I2C_init();

}

int main(void){
    RCC->APB2ENR = RCC_APB2ENR_IOPBEN;
    setup();

    uint8_t mem_addr = 0x0;
    uint8_t w_data = 0b01010101;
    uint8_t r_data = 0x0;
    uint8_t c_data = 0x0;

    write_eeprom_data(mem_addr, w_data);
    delay();
    r_data = read_eeprom_data(mem_addr);

    c_data = w_data & r_data;
    if (c_data == w_data) {
        digitalWrite(GPIOB, 1, HIGH);
    }

    while(1) {
    }
}
